# vis-bpts

A simple vis plugin that reworks insert mode to use bullet-points. Primarily useful for the way I personally take notes.

## Functions
- `bullets()`: Go through all the bullets and give them the correct symbols. Does not work if you make a block of bullets more then one indentation level more indented then the previous, but standard formatting works without issue.
- `indent()`: Increase the indentation level of the current line.
- `unindent()`: Decrease the indentation level of the current line.
- `init()`: Bind all the below listed keybinds.

## Insert mode
- `<Tab>`: Increase the indentation level of the current line.
- `<S-Tab>`: Decrease the indentation level of the current line.
- `<Enter>`: Insert a newline and add appropriate level of indentation, creating a bullet point.
- `<S-Enter>`: Regular newline.

## Normal mode
- `<Tab>`: Run `bullets()`.

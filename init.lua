require("vis")

local bpts = {}

bpts.bullets = function() -- recalculate bullet symbols
	local win = vis.win
	local startloc = win.selection.pos; local startmode = vis.mode;
	local letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" -- alphabet used for the 2nd indentation level
	vis:command(", x/^(\t)*([0-9]|[A-Z]|[a-z])\\. / x/[0-9]|[A-Z]|[a-z]/") -- select all bullets symbols
	local runs = {}; setmetatable(runs,{__index = function () return 0 end}) -- keep track of how long each "run" of a certain indentation level is
	local oldcol = 1 -- keep track of the previous collumn
	for s in win:selections_iterator() do
		runs[s.col] = runs[s.col]+1
		if s.col < oldcol then for i = s.col+1,#runs do table.remove(runs) end end -- when the indentation level decreases, end runs of all greater indentation levels
		oldcol = s.col
		win.file:insert(s.pos,(s.col == 1) and runs[1] or (s.col == 2) and letters:sub(runs[2],runs[2]) or string.lower(letters:sub(runs[s.col],runs[s.col]))) -- insert the symbol corresponding to current col and run length
		win.file:delete(s.pos,1) -- remove old symbol
	end
	win.selection.pos = startloc; vis:feedkeys("<vis-mode-normal-escape>"); vis.mode = startmode;
end

bpts.indent = function()
	local win = vis.win
	local position = win.selection.pos+1
	win.file.lines[win.selection.line] = "	"..win.file.lines[win.selection.line] -- add a tab to the start of the line
	win.selection.pos = position -- update position to compensate for increased line length
	bpts:bullets() -- update bullet symbols
end

bpts.unindent = function()
	local win = vis.win
	if win.file.lines[win.selection.line]:sub(1,1) == "	" then -- if line starts with a tab
		local position = win.selection.pos-1
		win.file.lines[win.selection.line] = win.file.lines[win.selection.line]:sub(2) -- remove first char in line
		win.selection.pos = position -- update position to compensate for decreased line length
		bpts:bullets() -- update bullet symbols
	end
end

bpts.init = function()
	vis:map(vis.modes.INSERT, "<Tab>", function() bpts:indent() end)
	vis:map(vis.modes.INSERT, "<S-Tab>", function() bpts:unindent() end)
	vis:map(vis.modes.INSERT, "<Enter>", function() vis:feedkeys("<vis-motion-line-end><vis-insert-newline>1. "); bpts:bullets() end) -- add another line, adding a dummy bullet symbol and fixing it
	vis:map(vis.modes.INSERT, "<S-Enter>", "<vis-insert-newline>")
	vis:map(vis.modes.NORMAL, "<Tab>", function() bpts:bullets() end)
	vis.options.ai = true -- needed for proper functionality of <Enter>, also feels nice
end

return bpts
